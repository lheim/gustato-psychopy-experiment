#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v3.0.0b12),
    on Mon Apr 22 19:37:45 2019
If you publish work using this script please cite the PsychoPy publications:
    Peirce, JW (2007) PsychoPy - Psychophysics software in Python.
        Journal of Neuroscience Methods, 162(1-2), 8-13.
    Peirce, JW (2009) Generating stimuli for neuroscience using PsychoPy.
        Frontiers in Neuroinformatics, 2:10. doi: 10.3389/neuro.11.010.2008
"""

from __future__ import absolute_import, division
from psychopy import locale_setup, sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '3.0.0b12'
expName = 'gustato-congruence'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001'}
dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='/Users/nope/ownCloud/work/uka/code/GUSTATO-2019/gustato-congruence-2019/gustato-congruence.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[1920, 1080], fullscr=False, screen=1,
    allowGUI=True, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True,
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# Initialize components for Routine "welcome"
welcomeClock = core.Clock()
welcome_text = visual.TextStim(win=win, name='welcome_text',
    text='Herzlich Willkommen zum Experiment!\n\n\nweiter mit Leertaste',
    font='Arial',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "instruction_1"
instruction_1Clock = core.Clock()
text_instruction = visual.TextStim(win=win, name='text_instruction',
    text='Im Folgenden wird entweder das Bild einer Zitrone oder das Bild von Zuckerwürfeln auf dem Bildschirm erscheinen. Parallel dazu wird Ihnen über die Schläuche ein süßer oder saurer Geschmack präsentiert.\n\nweiter mit der Leertaste\n',
    font='Arial',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "instruction_2"
instruction_2Clock = core.Clock()
text_instruction_2 = visual.TextStim(win=win, name='text_instruction_2',
    text='Ihre Aufgabe ist es zu entscheiden, ob das Bild oder der Geschmack sauer oder süß ist. \nAuf einen sauren Reiz reagieren sie bitte mit der Taste "Y" und auf einen süßen Reiz mit der Taste "N".\nDas Bild und der Geschmack können dabei entweder übereinstimmen oder nicht. \nDeshalb sollen Sie in jedem Durchgang entweder auf das Bild oder auf den Geschmack reagieren.\n\nweiter mit der Leertase\n',
    font='Arial',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "instruction_3"
instruction_3Clock = core.Clock()
text_instruction_3 = visual.TextStim(win=win, name='text_instruction_3',
    text='Um zu wissen, ob sie auf das Bild oder den Geschmack reagieren sollen, hören sie zunächst entweder das Wort "Bild" oder das Wort "Geschmack".\n\nWenn sie das Wort "Bild" hören, sollen sie im folgenden Durchgang auf das Bild reagieren. \nWenn sie das Wort "Geschmack" hören, sollen sie im folgenden Durchgang auf den Geschmack reagieren.\n\nweiter mit der Leertaste\n',
    font='Arial',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "instruction_4"
instruction_4Clock = core.Clock()
text_instruction_4 = visual.TextStim(win=win, name='text_instruction_4',
    text='Noch einmal zur Erinnerung:\n\nZunächst hören sie gleich entweder das Wort "Bild" oder "Geschmack". Danach wird ihnen gleichzeitig ein visueller Reiz und ein Geschmacksreiz präsentiert.\nWenn sie das Wort "Bild" hören reagieren sie bitte auf den visuellen Reiz und wenn sie das Wort "Geschmack" hören bitte auf den Geschmacksreiz.\nSie sollen dabei entscheiden, ob der Reiz süß oder sauer ist.\nMit der Taste "Y" geben sie an, dass der Reiz sauer ist und mit der Taste "N", dass er süß ist.\n\nweiter mit der Leertaste',
    font='Arial',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "instruction_5"
instruction_5Clock = core.Clock()
text_instruction_5 = visual.TextStim(win=win, name='text_instruction_5',
    text='Bitte antworten Sie immer so schnell und so genau wie möglich.\n\n\nDrücken sie die Leertaste, wenn sie bereit sind das Experiment zu starten\n\n',
    font='Arial',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "gustato_init"
gustato_initClock = core.Clock()
import serial 
from datetime import datetime 
from threading import Lock, Thread, Event 
import time

gustato_port = '/dev/cu.wchusbserial1410'

try:
    gustato_com = serial.Serial(gustato_port, baudrate=115200)
    print("Serial: Found serial interface, using it.")
except serial.SerialException:
    print("Serial: Couldnt find serial interface. Using port = None.")
    trigger = None 

# Initialize components for Routine "gustato_communicator"
gustato_communicatorClock = core.Clock()


# Initialize components for Routine "cue_routine"
cue_routineClock = core.Clock()
sound_1 = sound.Sound('A', secs=2, stereo=True)
sound_1.setVolume(1)

# Initialize components for Routine "pause_1"
pause_1Clock = core.Clock()
pause_text_1 = visual.TextStim(win=win, name='pause_text_1',
    text=None,
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "stimuli"
stimuliClock = core.Clock()
pic_stimuli = visual.ImageStim(
    win=win, name='pic_stimuli',
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=(0.75, 0.5),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=0.0)

# Initialize components for Routine "pause"
pauseClock = core.Clock()
text_2 = visual.TextStim(win=win, name='text_2',
    text=None,
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "new_block"
new_blockClock = core.Clock()
weisser_hintergrund = visual.ImageStim(
    win=win, name='weisser_hintergrund',
    image='weiss.png', mask=None,
    ori=0, pos=(0, 0), size=(2,2),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=0.0)
text = visual.TextStim(win=win, name='text',
    text='Ende des Blocks\n\nBitte warten Sie auf den Versuchsleiter.',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);

# Initialize components for Routine "end"
endClock = core.Clock()
text_end = visual.TextStim(win=win, name='text_end',
    text='Vielen Dank für Ihre Teilnahme am Experiment!\n\nSie können sich jetzt an den Versuchsleiter wenden.',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);


# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "welcome"-------
t = 0
welcomeClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_4 = event.BuilderKeyResponse()
# keep track of which components have finished
welcomeComponents = [welcome_text, key_resp_4]
for thisComponent in welcomeComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "welcome"-------
while continueRoutine:
    # get current time
    t = welcomeClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *welcome_text* updates
    if t >= 0.0 and welcome_text.status == NOT_STARTED:
        # keep track of start time/frame for later
        welcome_text.tStart = t
        welcome_text.frameNStart = frameN  # exact frame index
        welcome_text.setAutoDraw(True)
    
    # *key_resp_4* updates
    if t >= 0.0 and key_resp_4.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_4.tStart = t
        key_resp_4.frameNStart = frameN  # exact frame index
        key_resp_4.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if key_resp_4.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in welcomeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "welcome"-------
for thisComponent in welcomeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "welcome" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "instruction_1"-------
t = 0
instruction_1Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_5 = event.BuilderKeyResponse()
# keep track of which components have finished
instruction_1Components = [text_instruction, key_resp_5]
for thisComponent in instruction_1Components:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instruction_1"-------
while continueRoutine:
    # get current time
    t = instruction_1Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_instruction* updates
    if t >= 0 and text_instruction.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_instruction.tStart = t
        text_instruction.frameNStart = frameN  # exact frame index
        text_instruction.setAutoDraw(True)
    
    # *key_resp_5* updates
    if t >= 0.0 and key_resp_5.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_5.tStart = t
        key_resp_5.frameNStart = frameN  # exact frame index
        key_resp_5.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if key_resp_5.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instruction_1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruction_1"-------
for thisComponent in instruction_1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "instruction_1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "instruction_2"-------
t = 0
instruction_2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_6 = event.BuilderKeyResponse()
# keep track of which components have finished
instruction_2Components = [text_instruction_2, key_resp_6]
for thisComponent in instruction_2Components:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instruction_2"-------
while continueRoutine:
    # get current time
    t = instruction_2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_instruction_2* updates
    if t >= 0 and text_instruction_2.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_instruction_2.tStart = t
        text_instruction_2.frameNStart = frameN  # exact frame index
        text_instruction_2.setAutoDraw(True)
    
    # *key_resp_6* updates
    if t >= 0.0 and key_resp_6.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_6.tStart = t
        key_resp_6.frameNStart = frameN  # exact frame index
        key_resp_6.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if key_resp_6.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instruction_2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruction_2"-------
for thisComponent in instruction_2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "instruction_2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "instruction_3"-------
t = 0
instruction_3Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_8 = event.BuilderKeyResponse()
# keep track of which components have finished
instruction_3Components = [text_instruction_3, key_resp_8]
for thisComponent in instruction_3Components:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instruction_3"-------
while continueRoutine:
    # get current time
    t = instruction_3Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_instruction_3* updates
    if t >= 0 and text_instruction_3.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_instruction_3.tStart = t
        text_instruction_3.frameNStart = frameN  # exact frame index
        text_instruction_3.setAutoDraw(True)
    
    # *key_resp_8* updates
    if t >= 0.0 and key_resp_8.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_8.tStart = t
        key_resp_8.frameNStart = frameN  # exact frame index
        key_resp_8.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if key_resp_8.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instruction_3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruction_3"-------
for thisComponent in instruction_3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "instruction_3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "instruction_4"-------
t = 0
instruction_4Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_9 = event.BuilderKeyResponse()
# keep track of which components have finished
instruction_4Components = [text_instruction_4, key_resp_9]
for thisComponent in instruction_4Components:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instruction_4"-------
while continueRoutine:
    # get current time
    t = instruction_4Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_instruction_4* updates
    if t >= 0.0 and text_instruction_4.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_instruction_4.tStart = t
        text_instruction_4.frameNStart = frameN  # exact frame index
        text_instruction_4.setAutoDraw(True)
    
    # *key_resp_9* updates
    if t >= 0.0 and key_resp_9.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_9.tStart = t
        key_resp_9.frameNStart = frameN  # exact frame index
        key_resp_9.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if key_resp_9.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instruction_4Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruction_4"-------
for thisComponent in instruction_4Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "instruction_4" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "instruction_5"-------
t = 0
instruction_5Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_7 = event.BuilderKeyResponse()
# keep track of which components have finished
instruction_5Components = [text_instruction_5, key_resp_7]
for thisComponent in instruction_5Components:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instruction_5"-------
while continueRoutine:
    # get current time
    t = instruction_5Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_instruction_5* updates
    if t >= 0 and text_instruction_5.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_instruction_5.tStart = t
        text_instruction_5.frameNStart = frameN  # exact frame index
        text_instruction_5.setAutoDraw(True)
    
    # *key_resp_7* updates
    if t >= 0 and key_resp_7.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_7.tStart = t
        key_resp_7.frameNStart = frameN  # exact frame index
        key_resp_7.status = STARTED
        # keyboard checking is just starting
        event.clearEvents(eventType='keyboard')
    if key_resp_7.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instruction_5Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruction_5"-------
for thisComponent in instruction_5Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "instruction_5" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "gustato_init"-------
t = 0
gustato_initClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat





# keep track of which components have finished
gustato_initComponents = []
for thisComponent in gustato_initComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "gustato_init"-------
while continueRoutine:
    # get current time
    t = gustato_initClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in gustato_initComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "gustato_init"-------
for thisComponent in gustato_initComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)

# the Routine "gustato_init" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
blocks = data.TrialHandler(nReps=3, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=[None],
    seed=None, name='blocks')
thisExp.addLoop(blocks)  # add the loop to the experiment
thisBlock = blocks.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisBlock.rgb)
if thisBlock != None:
    for paramName in thisBlock:
        exec('{} = thisBlock[paramName]'.format(paramName))

for thisBlock in blocks:
    print(thisBlock)
    currentLoop = blocks
    # abbreviate parameter names if possible (e.g. rgb = thisBlock.rgb)
    if thisBlock != None:
        for paramName in thisBlock:
            exec('{} = thisBlock[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "gustato_communicator"-------
    t = 0
    gustato_communicatorClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    def trigger_logging_thread(gustato_com, thisExp, blocks, logging):
    
        while True:
            current_line = str(gustato_com.readline())
            if '+++' in current_line:
                blocks.addData('gustato_trigger_unixtime', '%.3f' %time.time())
                logging.log(level=logging.EXP, msg= 'gustato_trigger_unixtime: ' + '%.3f' %time.time())
    
                blocks.addData('gustato_trigger_time', datetime.now().strftime("%H:%M:%S.%f"))
                logging.log(level=logging.EXP, msg= 'gustato_trigger_time: ' + datetime.now().strftime("%H:%M:%S.%f"))
    
                start_time = welcomeClock.getTime()
                blocks.addData('gustato_trigger_clock', start_time)
                logging.log(level=logging.EXP, msg= 'gustato_trigger_clock: %.2f' %start_time)
    
                next_line = str(gustato_com.readline());
                next_line = str(gustato_com.readline());
                counter = next_line[6:-5]
                blocks.addData('gustato_counter', counter)
                logging.log(level=logging.EXP, msg= 'gustato_counter: ' + counter)
    
                next_line = str(gustato_com.readline());
                voltage = next_line[11:-6]
                blocks.addData('gustato_voltage', voltage)
                logging.log(level=logging.EXP, msg= 'gustato_voltage: ' + voltage)
    
            elif '---' in current_line:
                next_line = str(gustato_com.readline());
                next_line = str(gustato_com.readline());
                elapsed_time = next_line[8:-7]
                blocks.addData('gustato_trigger_elapsed_time', elapsed_time)
                logging.log(level=logging.EXP, msg= 'gustato_trigger_elapsed_time: ' + elapsed_time)
    
            elif 'KILL trigger thread.' in current_line:
                break;
    
        print("Exiting trigger thread ...")
        
        
    if gustato_com:
        trigger_thread = Thread(target=trigger_logging_thread, args=[gustato_com, thisExp, blocks, logging])
        trigger_thread.start()
    # keep track of which components have finished
    gustato_communicatorComponents = []
    for thisComponent in gustato_communicatorComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "gustato_communicator"-------
    while continueRoutine:
        # get current time
        t = gustato_communicatorClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in gustato_communicatorComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "gustato_communicator"-------
    for thisComponent in gustato_communicatorComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # starting gustatometer by setting the trigger_in port to high
    if gustato_com:
        print("Serial: Sending 'START'")
        gustato_com.write(b'START')
    
        blocks.addData('gustato_init_unixtime', '%.3f' %time.time())
        logging.log(level=logging.EXP, msg= 'gustato_init_unixtime: ' + '%.3f' %time.time())
    
        blocks.addData('gustato_init_time', datetime.now().strftime("%H:%M:%S.%f"))
        logging.log(level=logging.EXP, msg= 'gustato_init_time: ' + datetime.now().strftime("%H:%M:%S.%f"))
    
        start_time = welcomeClock.getTime()
        blocks.addData('gustato_init_clock', start_time)
        logging.log(level=logging.EXP, msg= 'gustato_init_clock: %.2f' %start_time)
    
        time.sleep(2)
        print("Serial: Sending 'STOP'")
        gustato_com.write(b'STOP')
    # the Routine "gustato_communicator" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    sequence = data.TrialHandler(nReps=1, method='sequential', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('sequence_short.xlsx'),
        seed=None, name='sequence')
    thisExp.addLoop(sequence)  # add the loop to the experiment
    thisSequence = sequence.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisSequence.rgb)
    if thisSequence != None:
        for paramName in thisSequence:
            exec('{} = thisSequence[paramName]'.format(paramName))
    
    for thisSequence in sequence:
        currentLoop = sequence
        # abbreviate parameter names if possible (e.g. rgb = thisSequence.rgb)
        if thisSequence != None:
            for paramName in thisSequence:
                exec('{} = thisSequence[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "cue_routine"-------
        t = 0
        cue_routineClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(2.000000)
        # update component parameters for each repeat
        sound_1.setSound(cue, secs=2)
        sound_1.setVolume(1, log=False)
        # keep track of which components have finished
        cue_routineComponents = [sound_1]
        for thisComponent in cue_routineComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "cue_routine"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = cue_routineClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            # start/stop sound_1
            if t >= 0.0 and sound_1.status == NOT_STARTED:
                # keep track of start time/frame for later
                sound_1.tStart = t
                sound_1.frameNStart = frameN  # exact frame index
                win.callOnFlip(sound_1.play)  # screen flip
            frameRemains = 0.0 + 2- win.monitorFramePeriod * 0.75  # most of one frame period left
            if sound_1.status == STARTED and t >= frameRemains:
                sound_1.stop()  # stop the sound (if longer than duration)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in cue_routineComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "cue_routine"-------
        for thisComponent in cue_routineComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        sound_1.stop()  # ensure sound has stopped at end of routine
        
        # ------Prepare to start Routine "pause_1"-------
        t = 0
        pause_1Clock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(2.000000)
        # update component parameters for each repeat
        # keep track of which components have finished
        pause_1Components = [pause_text_1]
        for thisComponent in pause_1Components:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "pause_1"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = pause_1Clock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *pause_text_1* updates
            if t >= 0.0 and pause_text_1.status == NOT_STARTED:
                # keep track of start time/frame for later
                pause_text_1.tStart = t
                pause_text_1.frameNStart = frameN  # exact frame index
                pause_text_1.setAutoDraw(True)
            frameRemains = 0.0 + 2- win.monitorFramePeriod * 0.75  # most of one frame period left
            if pause_text_1.status == STARTED and t >= frameRemains:
                pause_text_1.setAutoDraw(False)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in pause_1Components:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "pause_1"-------
        for thisComponent in pause_1Components:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        
        # ------Prepare to start Routine "stimuli"-------
        t = 0
        stimuliClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(6.000000)
        # update component parameters for each repeat
        pic_stimuli.setImage(picture)
        key_stimuli_response = event.BuilderKeyResponse()
        # keep track of which components have finished
        stimuliComponents = [pic_stimuli, key_stimuli_response]
        for thisComponent in stimuliComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "stimuli"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = stimuliClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *pic_stimuli* updates
            if t >= 0 and pic_stimuli.status == NOT_STARTED:
                # keep track of start time/frame for later
                pic_stimuli.tStart = t
                pic_stimuli.frameNStart = frameN  # exact frame index
                pic_stimuli.setAutoDraw(True)
            frameRemains = 0 + 2- win.monitorFramePeriod * 0.75  # most of one frame period left
            if pic_stimuli.status == STARTED and t >= frameRemains:
                pic_stimuli.setAutoDraw(False)
            
            # *key_stimuli_response* updates
            if t >= 0 and key_stimuli_response.status == NOT_STARTED:
                # keep track of start time/frame for later
                key_stimuli_response.tStart = t
                key_stimuli_response.frameNStart = frameN  # exact frame index
                key_stimuli_response.status = STARTED
                # keyboard checking is just starting
                win.callOnFlip(key_stimuli_response.clock.reset)  # t=0 on next screen flip
                event.clearEvents(eventType='keyboard')
            frameRemains = 0 + 6- win.monitorFramePeriod * 0.75  # most of one frame period left
            if key_stimuli_response.status == STARTED and t >= frameRemains:
                key_stimuli_response.status = STOPPED
            if key_stimuli_response.status == STARTED:
                theseKeys = event.getKeys(keyList=['y', 'n'])
                
                # check for quit:
                if "escape" in theseKeys:
                    endExpNow = True
                if len(theseKeys) > 0:  # at least one key was pressed
                    key_stimuli_response.keys = theseKeys[-1]  # just the last key pressed
                    key_stimuli_response.rt = key_stimuli_response.clock.getTime()
                    # was this 'correct'?
                    if (key_stimuli_response.keys == str(correctanswer)) or (key_stimuli_response.keys == correctanswer):
                        key_stimuli_response.corr = 1
                    else:
                        key_stimuli_response.corr = 0
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in stimuliComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "stimuli"-------
        for thisComponent in stimuliComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        # check responses
        if key_stimuli_response.keys in ['', [], None]:  # No response was made
            key_stimuli_response.keys=None
            # was no response the correct answer?!
            if str(correctanswer).lower() == 'none':
               key_stimuli_response.corr = 1;  # correct non-response
            else:
               key_stimuli_response.corr = 0;  # failed to respond (incorrectly)
        # store data for sequence (TrialHandler)
        sequence.addData('key_stimuli_response.keys',key_stimuli_response.keys)
        sequence.addData('key_stimuli_response.corr', key_stimuli_response.corr)
        if key_stimuli_response.keys != None:  # we had a response
            sequence.addData('key_stimuli_response.rt', key_stimuli_response.rt)
        
        # ------Prepare to start Routine "pause"-------
        t = 0
        pauseClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(3.500000)
        # update component parameters for each repeat
        # keep track of which components have finished
        pauseComponents = [text_2]
        for thisComponent in pauseComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        
        # -------Start Routine "pause"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = pauseClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *text_2* updates
            if t >= 0.0 and text_2.status == NOT_STARTED:
                # keep track of start time/frame for later
                text_2.tStart = t
                text_2.frameNStart = frameN  # exact frame index
                text_2.setAutoDraw(True)
            frameRemains = 0.0 + 3.5- win.monitorFramePeriod * 0.75  # most of one frame period left
            if text_2.status == STARTED and t >= frameRemains:
                text_2.setAutoDraw(False)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in pauseComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "pause"-------
        for thisComponent in pauseComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.nextEntry()
        
    # completed 1 repeats of 'sequence'
    
    
    # ------Prepare to start Routine "new_block"-------
    t = 0
    new_blockClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    block_continue = event.BuilderKeyResponse()
    # keep track of which components have finished
    new_blockComponents = [weisser_hintergrund, text, block_continue]
    for thisComponent in new_blockComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "new_block"-------
    while continueRoutine:
        # get current time
        t = new_blockClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *weisser_hintergrund* updates
        if t >= 0.0 and weisser_hintergrund.status == NOT_STARTED:
            # keep track of start time/frame for later
            weisser_hintergrund.tStart = t
            weisser_hintergrund.frameNStart = frameN  # exact frame index
            weisser_hintergrund.setAutoDraw(True)
        
        # *text* updates
        if t >= 0.0 and text.status == NOT_STARTED:
            # keep track of start time/frame for later
            text.tStart = t
            text.frameNStart = frameN  # exact frame index
            text.setAutoDraw(True)
        
        # *block_continue* updates
        if t >= 0.0 and block_continue.status == NOT_STARTED:
            # keep track of start time/frame for later
            block_continue.tStart = t
            block_continue.frameNStart = frameN  # exact frame index
            block_continue.status = STARTED
            # keyboard checking is just starting
            block_continue.clock.reset()  # now t=0
            event.clearEvents(eventType='keyboard')
        if block_continue.status == STARTED:
            theseKeys = event.getKeys(keyList=['u'])
            
            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                block_continue.keys = theseKeys[-1]  # just the last key pressed
                block_continue.rt = block_continue.clock.getTime()
                # a response ends the routine
                continueRoutine = False
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in new_blockComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "new_block"-------
    for thisComponent in new_blockComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if block_continue.keys in ['', [], None]:  # No response was made
        block_continue.keys=None
    blocks.addData('block_continue.keys',block_continue.keys)
    if block_continue.keys != None:  # we had a response
        blocks.addData('block_continue.rt', block_continue.rt)
    # the Routine "new_block" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 3 repeats of 'blocks'


# ------Prepare to start Routine "end"-------
t = 0
endClock.reset()  # clock
frameN = -1
continueRoutine = True
routineTimer.add(5.000000)
# update component parameters for each repeat

# keep track of which components have finished
endComponents = [text_end]
for thisComponent in endComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "end"-------
while continueRoutine and routineTimer.getTime() > 0:
    # get current time
    t = endClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_end* updates
    if t >= 0.0 and text_end.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_end.tStart = t
        text_end.frameNStart = frameN  # exact frame index
        text_end.setAutoDraw(True)
    frameRemains = 0.0 + 5- win.monitorFramePeriod * 0.75  # most of one frame period left
    if text_end.status == STARTED and t >= frameRemains:
        text_end.setAutoDraw(False)
    
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in endComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "end"-------
for thisComponent in endComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
if gustato_com: 
    print("Serial: Sending 'KILL'")
    gustato_com.write(b'KILL')     
    trigger_thread.join() 
if gustato_com:
    gustato_com.close()


# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
