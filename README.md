# Gustatometer Congruence PsychoPy Experiment



## Gustatometer PsychoPy Communicator
Use the following PlatformIO code for a ESP8266: https://gitlab.com/lheim/gustatometer-psychopy-communicator
This repository (https://gitlab.com/lheim/gustato-psychopy-experiment) contains a sample PsychoPy experiment which communicates with the ESP8266 and logs the 'trigger-out' responses.

![Microcontroller Setup](microcontroller-setup.jpg)


### Driver
Install the following CH340G driver and note the COM port. Change the COM port in the PsychoPy Code block.

https://sparks.gogo.co.nz/ch340.html
